package com.superfling.challenge.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import com.superfling.challenge.tasks.ReportGeneratorTask;

/**
 * Report generator service
 * @author Yanislav
 */
public class ReportService extends IntentService {

	/**
	 * Constructor
	 */
	public ReportService() {
		super(ReportService.class.getSimpleName());
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ReportGeneratorTask task = new ReportGeneratorTask(getApplicationContext());
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
}
