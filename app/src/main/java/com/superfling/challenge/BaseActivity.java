package com.superfling.challenge;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * This is the base activity that has to be implemented by all the activities in the application
 * @author Yanislav Mihaylov
 */
public class BaseActivity extends ActionBarActivity {
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
	}
}
