package com.superfling.challenge.utils;

import org.apache.http.Header;
import org.apache.http.StatusLine;

/**
 * Http Response object that is returned by {@link IHttpCallHelper}
 * @author Yanislav
 */
public class HttpResponse {
	/** Status Line of the Http Response */
	private StatusLine mStatusLine;
	/** Response Headers */
	private Header[] mHeaders;
	/** Returned response as a String */
	private String mData;

	/**
	 * Constructor
	 * @param statusLine Status Line of the Response
	 * @param headers Response Headers
	 * @param data Returned Data as a {@link String}
	 */
	public HttpResponse(final StatusLine statusLine, final Header[] headers, final String data) {
		mStatusLine = statusLine;
		mHeaders = headers;
		mData = data;
	}

	/**
	 * Gets the Status Line of the response
	 * @return Status Line of the response
	 */
	public StatusLine getStatusLine() {
		return mStatusLine;
	}

	/**
	 * Gets the returned data as a {@link String}
	 * @return Returned Data
	 */
	public String getData() {
		return mData;
	}

	/**
	 * Gets the response headers
	 * @return Response Headers
	 */
	public Header[] getHeaders() {
		return mHeaders;
	}
}
