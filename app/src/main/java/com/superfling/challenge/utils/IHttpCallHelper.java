package com.superfling.challenge.utils;

import java.io.IOException;

/**
 * Makes the Http calls
 * @author Yanislav
 */
public interface IHttpCallHelper {
	/**
	 * Makes a Get call to the specified URL and returns the response
	 * @param url URL that needs to be called
	 * @return {@link HttpResponse}
	 * @throws IOException Exception that is thrown in case of a network communication error
	 */
	HttpResponse getData(final String url) throws IOException;

	/**
	 * Closes the Http Client
	 */
	void close();
}
