package com.superfling.challenge.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.annotation.NonNull;

import com.squareup.picasso.Transformation;
import com.superfling.challenge.model.ImageData;

/**
 * Resize proportionally the image according to the Max Image Width
 * @author Yanislav
 */
public class PicassoImageTransformation implements Transformation {
	/**
	 * Callback interface that provides back the image size
	 */
	public interface IImageResizedCallback {
		/**
		 * Callback method that is called once the image is resized
		 * @param imageData Image data of the image that is resized
		 * @param originalImageWidth Original Image Width
		 * @param originalImageHeight Original Image Height
		 * @param originalImageSize Original Image size in Bytes
		 * @param newImageWidth New Image Width
		 * @param newImageHeight New Image Height
		 */
		void onImageResized(final ImageData imageData, final int originalImageWidth, final int originalImageHeight, final int originalImageSize,
							final int newImageWidth, final int newImageHeight);
	}


	/** Image data for which the transformation is triggered */
	private ImageData mImageData;
	/** Image Maximum Width that will be used for resizing the image */
	private int mMaxImageWidth;
	private IImageResizedCallback mCallback;

	/**
	 * Constructor
	 * @param imageData {@link ImageData} for which the transformation is triggered
	 * @param maxImageWidth Image Maximum Width that will be used for resizing the image
	 */
	public PicassoImageTransformation(final ImageData imageData, final int maxImageWidth, @NonNull final IImageResizedCallback callback) {
		mImageData = imageData;
		mMaxImageWidth = maxImageWidth;
		mCallback = callback;
	}

	@Override
	public Bitmap transform(final Bitmap source) {
		Bitmap result = source;
		if (source.getWidth() > mMaxImageWidth) {
			final float transformFactor =  (float) mMaxImageWidth / source.getWidth();

			Matrix matrix = new Matrix();
			matrix.postScale(transformFactor, transformFactor);

			result = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, false);
			mCallback.onImageResized(mImageData, source.getWidth(), source.getHeight(), source.getByteCount(),
					result.getWidth(), result.getHeight());
		} else {
			mCallback.onImageResized(mImageData, source.getWidth(), source.getHeight(), source.getByteCount(),
					source.getWidth(), source.getHeight());
		}

		if (result != source) {
			source.recycle();
		}

		return result;
	}

	@Override
	public String key() {
		return "picassoImageTransformation()";
	}
}
