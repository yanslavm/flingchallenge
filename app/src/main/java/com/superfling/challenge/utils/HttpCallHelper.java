package com.superfling.challenge.utils;

import android.content.Context;
import android.net.http.AndroidHttpClient;

import org.apache.http.client.methods.HttpGet;

import java.io.IOException;

/**
 * Makes the Http Calls in order to make it easier to create tests that to mock the calls
 * @author Yanislav
 */
public class HttpCallHelper implements IHttpCallHelper {
	/** Instance of {@link AndroidHttpClient} */
	private final AndroidHttpClient mHttpClient;

	/**
	 * Constructor
	 * @param context Context
	 */
	public HttpCallHelper(final Context context) {
		mHttpClient = AndroidHttpClient.newInstance("FlingChallenge", context);
	}

	@Override
	public HttpResponse getData(String url) throws IOException {
		HttpGet httpGet = new HttpGet(url);
		org.apache.http.HttpResponse result = mHttpClient.execute(httpGet);
		String data = Utils.convertStreamToString(result.getEntity().getContent());

		return new HttpResponse(result.getStatusLine(), result.getAllHeaders(), data);
	}

	@Override
	public void close() {
		mHttpClient.close();
	}
}
