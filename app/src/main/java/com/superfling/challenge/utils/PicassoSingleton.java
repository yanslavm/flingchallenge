package com.superfling.challenge.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.superfling.challenge.BuildConfig;

/**
 * @author Yanislav
 */
public class PicassoSingleton {

	private static final String TAG = PicassoSingleton.class.getSimpleName();
	/** Singleton instance of {@link Picasso} */
	private static Picasso sPicasso;
	/** Singleton instance of {@link LruCache} */
	private static LruCache sLruCache;

	/**
	 * Gets a singleton instance of {@link LruCache}
	 * @param context Application Context
	 * @return Singleton instance of {@link LruCache}
	 */
	public static synchronized  LruCache getLruCache(final Context context) {
		if (sLruCache == null) {
			final ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			final int maxHeapInMB = am.getMemoryClass();
			Log.v(TAG, "Maximum memory :" + Integer.toString(maxHeapInMB));

			sLruCache = new LruCache(maxHeapInMB*1024*1024/2);
		}

		return sLruCache;
	}

	/**
	 * Gets a singleton Picasso instance
	 * @param context Application Context
	 * @return Picasso instance
	 */
	public static synchronized Picasso getPicasso(final Context context) {
		if (sPicasso == null) {
			final Picasso.Builder builder = new Picasso.Builder(context.getApplicationContext());
			builder.memoryCache(getLruCache(context));
			builder.indicatorsEnabled(BuildConfig.DEBUG);
			sPicasso = builder.build();
		}

		return sPicasso;
	}
}
