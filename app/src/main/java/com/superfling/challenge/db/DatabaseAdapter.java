package com.superfling.challenge.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.superfling.challenge.model.ImageData;
import com.superfling.challenge.model.ReportData;

import java.util.ArrayList;
import java.util.List;

/**
 * Database adapter class. Provides methods for storing and reading data to and from Database
 */
public class DatabaseAdapter {
	/** Application Context */
	private final Context mContext;
	/** Database Helper */
	private DatabaseHelper dbHelper;
	/** Instance of {@link SQLiteDatabase} */
	private SQLiteDatabase database;
	/** Singleton instance */
	private static DatabaseAdapter sInstance;

	/**
	 * Get singleton instance
	 * @param context Application Context
	 * @return Singleton instance
	 */
	public static synchronized DatabaseAdapter getInstance(final Context context) {
		if (sInstance == null) {
			sInstance = new DatabaseAdapter(context);
		}

		return sInstance;
	}

	/**
	 * Constructor
	 * @param context Application Context
	 */
	private DatabaseAdapter(final Context context) {
		this.mContext = context;
	}

	/**
	 * Open Database
	 */
	private void open() {		
		dbHelper = new DatabaseHelper(mContext);
		database = dbHelper.getWritableDatabase();
	}

	/**
	 * Closes Database
	 */
	private void close() {
		dbHelper.close();
		dbHelper = null;
	}

	/**
	 * Gets all the ImageData as an {@link List}
	 * @return ImageData list
	 */
	public synchronized List<ImageData> getAllImageDatas() {
		open();
		try {
			final Cursor cursor = database.query(DatabaseHelper.TABLE_IMAGE_DATA, null, null, null, null, null, DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_ID);
			try {
				final List<ImageData> result = new ArrayList<>();
				if (cursor.getCount() > 0) {
					final int columnId = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_ID);
					final int columnImageId = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_ID);
					final int columnTitle = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_TITLE);
					final int columnUserId = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_USER_ID);
					final int columnUserName = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_USER_NAME);
					final int columnImageSize = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_SIZE);
					final int columnImageWidth = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_WIDTH);
					final int columnImageHeight = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_HEIGHT);

					cursor.moveToFirst();
					do {
						final ImageData imageData = new ImageData();
						imageData.setId(cursor.getInt(columnId));
						imageData.setImageId(cursor.getInt(columnImageId));
						imageData.setTitle(cursor.getString(columnTitle));
						imageData.setUserId(cursor.getInt(columnUserId));
						imageData.setUserName(cursor.getString(columnUserName));
						imageData.setImageSize(cursor.getInt(columnImageSize));
						imageData.setImageWidth(cursor.getInt(columnImageWidth));
						imageData.setImageHeight(cursor.getInt(columnImageHeight));

						result.add(imageData);
					} while (cursor.moveToNext());
				}

				return result;
			} finally {
				cursor.close();
			}
		} finally {
			close();
		}
	}

	/**
	 * Saves the List of {@link ImageData} into the database. The old values will be removed and only the new one
	 * will be available
	 * @param imageDatas List of {@link ImageData}
	 */
	public synchronized void saveImageDatas(List<ImageData> imageDatas) {
		open();
		try {
			for (final ImageData imageData: imageDatas) {
				final ContentValues contentValues = createImageDataContentValues(imageData);
				final Cursor cursor = database.query(DatabaseHelper.TABLE_IMAGE_DATA, null, DatabaseHelper.FIELD_IMAGE_DATA_ID + "=?",
						new String[]{String.valueOf(imageData.getId())}, null, null, null);

				final boolean itemExists = cursor.getCount() > 0;
				cursor.close();

				if (!itemExists) {
					database.insert(DatabaseHelper.TABLE_IMAGE_DATA, /* nullColumnHack */ null, contentValues);
				}
			}
		} finally {
			close();
		}
	}

	/**
	 * Updates the Image Data
	 * @param imageData Image data that to be updated
	 */
	public synchronized void updateImageData(ImageData imageData) {
		open();
		try {
			final ContentValues contentValues = createImageDataContentValues(imageData);
			database.update(DatabaseHelper.TABLE_IMAGE_DATA, contentValues, DatabaseHelper.FIELD_IMAGE_DATA_ID + "=?",
					new String[] {String.valueOf(imageData.getId())});
		} finally {
			close();
		}
	}

	/**
	 * Creates {@link ContentValues} based on the information from {@link ImageData}
	 * @param imageData Image data
	 * @return {@link ContentValues} based on the information from {@link ImageData}
	 */
	private ContentValues createImageDataContentValues(final ImageData imageData) {
		final ContentValues result = new ContentValues();
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_ID, imageData.getId());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_ID, imageData.getImageId());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_TITLE, imageData.getTitle());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_USER_ID, imageData.getUserId());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_USER_NAME, imageData.getUserName());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_SIZE, imageData.getImageSize());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_WIDTH, imageData.getImageWidth());
		result.put(DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_HEIGHT, imageData.getImageHeight());

		return result;
	}

	/**
	 * Generates a summary for the images count, average images size and maximum image width by user
	 * @return List of {@link ReportData}
	 */
	public synchronized List<ReportData> generateReportData() {
		List<ReportData> result = new ArrayList<>();
		open();
		Cursor cursor = null;
		try {
			String sql = "SELECT " + DatabaseHelper.FIELD_IMAGE_DATA_USER_ID + ", "
					+ DatabaseHelper.FIELD_IMAGE_DATA_USER_NAME + ", "
					+ " COUNT(*) as posts_count, "
					+ " AVG(" + DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_SIZE + ") as avg_size, "
					+ " MAX(" + DatabaseHelper.FIELD_IMAGE_DATA_IMAGE_WIDTH + ") as max_width"
					+ " FROM " + DatabaseHelper.TABLE_IMAGE_DATA
					+ " GROUP BY " + DatabaseHelper.FIELD_IMAGE_DATA_USER_ID + ", "
					+ DatabaseHelper.FIELD_IMAGE_DATA_USER_NAME;

			cursor = database.rawQuery(sql, null);
			if (cursor.getCount() > 0) {
				final int columnUserId = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_USER_ID);
				final int columnUserName = cursor.getColumnIndex(DatabaseHelper.FIELD_IMAGE_DATA_USER_NAME);
				final int columnPostsCount = cursor.getColumnIndex("posts_count");
				final int columnAvgSize = cursor.getColumnIndex("avg_size");
				final int columnMaxWidth = cursor.getColumnIndex("max_width");

				cursor.moveToFirst();
				do {
					final ReportData reportData = new ReportData();
					reportData.setUserId(cursor.getInt(columnUserId));
					reportData.setUserName(cursor.getString(columnUserName));
					reportData.setPostsCount(cursor.getInt(columnPostsCount));
					reportData.setAverageImageSize(cursor.getInt(columnAvgSize));
					reportData.setMaxImageWidth(cursor.getInt(columnMaxWidth));

					result.add(reportData);
				} while (cursor.moveToNext());
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			close();
		}
		return result;
	}
}
