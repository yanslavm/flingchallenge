package com.superfling.challenge.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Database Helper class that manages the Database and creates and updates the table structure
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	/** Database Name */
	private static final String DATABASE_NAME = "FlingChallenge.db";
	/** Database Version */
	private static final int DATABASE_VERSION = 1;

	/** Image Data Table */
	public static final String TABLE_IMAGE_DATA = "image_data";

	/** Field for Image Data ID */
	public static final String FIELD_IMAGE_DATA_ID = "id";
	/** Field for Image ID */
	public static final String FIELD_IMAGE_DATA_IMAGE_ID = "image_id";
	/** Field for Title */
	public static final String FIELD_IMAGE_DATA_TITLE = "title";
	/** Field for User ID */
	public static final String FIELD_IMAGE_DATA_USER_ID = "user_id";
	/** Field for User Name */
	public static final String FIELD_IMAGE_DATA_USER_NAME = "user_name";
	/** Field for Image Size in Bytes */
	public static final String FIELD_IMAGE_DATA_IMAGE_SIZE = "image_size";
	/** Field for Image Width */
	public static final String FIELD_IMAGE_DATA_IMAGE_WIDTH = "image_width";
	/** Field for Image Height */
	public static final String FIELD_IMAGE_DATA_IMAGE_HEIGHT = "image_height";

	/** Creation script for Image Data */
	private static final String DATABASE_CREATE_IMAGE_DATA = "create table "+ TABLE_IMAGE_DATA +" ("
			+ FIELD_IMAGE_DATA_ID+" integer primary key, "
			+ FIELD_IMAGE_DATA_IMAGE_ID + " integer not null, "
			+ FIELD_IMAGE_DATA_IMAGE_SIZE + " integer not null, "
			+ FIELD_IMAGE_DATA_IMAGE_WIDTH + " integer not null, "
			+ FIELD_IMAGE_DATA_IMAGE_HEIGHT + " integer not null, "
			+ FIELD_IMAGE_DATA_TITLE + " text not null, "
			+ FIELD_IMAGE_DATA_USER_ID + " integer not null, "
			+ FIELD_IMAGE_DATA_USER_NAME + " text not null );";

	/**
	 * Constructor
	 * @param context Context
	 */
	public DatabaseHelper(final Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE_IMAGE_DATA);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database,  int oldVersion,	int newVersion) {
		Log.w(DatabaseHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "+ newVersion + ", which will destroy all old data");

	}
}
