package com.superfling.challenge.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.superfling.challenge.Config;
import com.superfling.challenge.R;
import com.superfling.challenge.model.ImageData;
import com.superfling.challenge.tasks.ImageSizeUpdateTask;
import com.superfling.challenge.utils.PicassoImageTransformation;
import com.superfling.challenge.utils.PicassoSingleton;

/**
 * Image Adapted View Holder
 * @author Yanislav
 */
public class ImageAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private static final String TAG = ImageAdapterViewHolder.class.getSimpleName();
	/** Progress bar that is shown during the image loading */
	private final ProgressBar mProgressBar;
	/** The Image View where the image is displayed */
	private final ImageView mImage;
	/** Title Text View */
	private final TextView mTitle;
	/** Max width of the Image. It is used as a parameter to Picasso in order to resize the image */
	private final int mMaxWidth;
	/** Context */
	private final Context mContext;
	/** Currently displayed ImageData */
	private ImageData mImageData;

	/** The callback handler that receives the image size after the image is resized */
	private PicassoImageTransformation.IImageResizedCallback mIImageResizedCallback = new PicassoImageTransformation.IImageResizedCallback() {
		@Override
		public void onImageResized(final ImageData imageData, final int originalImageWidth, final int originalImageHeight, final int originalImageSize,
								   final int newImageWidth, int newImageHeight) {
			Log.d(TAG, "onImageResized imageId: " + String.valueOf(imageData.getId())  + "; originalImageSize: " + String.valueOf(originalImageSize)
					+ "; originalImageWidth: " + String.valueOf(originalImageWidth) + "; originalImageHeight: " + String.valueOf(originalImageHeight)
					+ "; newImageWidth: " + String.valueOf(newImageWidth) + "; newImageHeight: " + String.valueOf(newImageHeight));

			if (imageData.getImageSize() == 0) {
				//triggers image size update only if the saved image size is 0 - which means that we still don't know it
				imageData.setImageWidth(originalImageWidth);
				imageData.setImageHeight(originalImageHeight);
				imageData.setImageSize(originalImageSize);

				ImageSizeUpdateTask task = new ImageSizeUpdateTask(mContext, imageData);
				task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}
		}
	};

	/**
	 * Image loaded callback class that will be notified once the image is loaded
	 */
	private class ImageLoadedCallback implements Callback {
		/** Image URL */
		private String mUrl;
		/** If to show the animation once the Image is Loaded */
		private boolean mShowAnimation;

		/**
		 * Constructor
		 * @param url URL of the image
		 */
		private ImageLoadedCallback(final String url, final boolean showAnimation) {
			mUrl = url;
			mShowAnimation = showAnimation;
		}

		@Override
		public void onSuccess() {
			if (mUrl.equals(Config.getImageUrl(mImageData.getImageId()))) {
				Log.d(TAG, "ImageLoadedCallback: onSuccess: Successfully loaded image with URL " + mUrl);
				mProgressBar.setVisibility(View.GONE);
				mImage.setVisibility(View.VISIBLE);

				if (mShowAnimation) {
					AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
					animation.setDuration(2000);

					mImage.startAnimation(animation);
				}
			} else {
				Log.d(TAG, "ImageLoadedCallback: onSuccess: URL CHANGED: Successfully loaded image with URL " + mUrl);
			}
		}

		@Override
		public void onError() {
			Log.w(TAG, "ImageLoadedCallback: OnError: Error during loading image for url: " + mUrl);
			String currentUrl = Config.getImageUrl(mImageData.getImageId());
			if (mUrl.equals(currentUrl)) {
				loadImage(currentUrl, mShowAnimation);
			}
		}
	}

	/**
	 * Constructor
	 * @param itemView Parent {@link View}
	 * @param width Parent width in pixels
	 */
	public ImageAdapterViewHolder(View itemView, int width) {
		super(itemView);
		mContext = itemView.getContext();
		mProgressBar = (ProgressBar) itemView.findViewById(R.id.row_image_preview_progress_bar);
		mImage = (ImageView) itemView.findViewById(R.id.row_image_preview_image);
		mTitle = (TextView) itemView.findViewById(R.id.row_image_preview_title);
		mMaxWidth = width;

		mImage.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (View.VISIBLE == mImage.getVisibility()) {
			mImage.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.image_clicked_animation));
		}
	}

	public void setImageData(final ImageData imageData) {
		mImageData = imageData;
		setTitle(mImageData.getTitle());
		setImageUrl(Config.getImageUrl(mImageData.getImageId()), !mImageData.isItemShown());
	}

	/**
	 * Sets the Title
	 * @param title Title
	 */
	private void setTitle(final String title) {
		mTitle.setText(title);
	}

	/**
	 * Sets the image URL
	 * @param url Image URL
	 * @param showAnimation If to show the animation once the image is loaded
	 */
	private void setImageUrl(final String url, final boolean showAnimation) {
		mProgressBar.setVisibility(View.VISIBLE);
		mImage.setVisibility(View.GONE);
		mImageData.setItemShown(true);

		loadImage(url, showAnimation);
	}

	/**
	 * Triggers Image Loading
	 * @param url URL of the image
	 * @param showAnimation If to show animation
	 */
	private void loadImage(String url, boolean showAnimation) {
		PicassoSingleton.getPicasso(mContext)
				.load(url)
				.transform(new PicassoImageTransformation(mImageData, mMaxWidth, mIImageResizedCallback))
				.into(mImage, new ImageLoadedCallback(url, showAnimation));
	}
}
