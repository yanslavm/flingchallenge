package com.superfling.challenge.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.superfling.challenge.R;
import com.superfling.challenge.model.ImageData;

import java.util.ArrayList;
import java.util.List;

/**
 * Image Adapter
 * @author Yanislav
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapterViewHolder> {
	private final List<ImageData> mImageDatas = new ArrayList<>();
	private final List<Integer> mImageIds = new ArrayList<>();

	@Override
	public ImageAdapterViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image_preview, parent, false);
		return new ImageAdapterViewHolder(v, parent.getWidth());
	}

	@Override
	public void onBindViewHolder(final ImageAdapterViewHolder holder, final int position) {
		holder.setImageData(mImageDatas.get(position));
	}

	@Override
	public int getItemCount() {
		return mImageDatas.size();
	}

	/**
	 * Sets the image data that has to be displayed
	 * @param imageDatas List of {@link ImageData}
	 */
	public void setImageDatas(final List<ImageData> imageDatas) {
		final List<Integer> newImageIds = new ArrayList<>();
		// Adds the new images to the list
		for (final ImageData imageData: imageDatas) {
			newImageIds.add(imageData.getId());
			if (!mImageIds.contains(imageData.getId())) {
				addItem(imageData);
			}
		}

		// Remove the missing images from the displayed list
		for (int i=mImageIds.size()-1; i >= 0; i--) {
			if (!newImageIds.contains(mImageIds.get(i))) {
				removeItem(i);
			}
		}
	}

	/**
	 * Remove item from the currently displayed items
	 * @param index Index of the item we want to remove
	 */
	private void removeItem(final int index) {
		mImageDatas.remove(index);
		mImageIds.remove(index);
		notifyItemRemoved(index);
	}

	/**
	 * Adds an ImageData to the list of currently displayed items
	 * @param imageData
	 */
	private void addItem(final ImageData imageData) {
		mImageDatas.add(imageData);
		mImageIds.add(imageData.getId());
		notifyItemInserted(mImageDatas.size() - 1);
	}
}
