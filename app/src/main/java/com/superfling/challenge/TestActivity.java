package com.superfling.challenge;

import android.app.Activity;

/**
 * Dumy test activity that is used in the instrumentation tests
 * @author Yanislav
 */
public class TestActivity extends Activity {
}
