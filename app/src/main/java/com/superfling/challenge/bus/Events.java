package com.superfling.challenge.bus;

/**
 * Contains the events that are triggered in the application
 * @author Yanislav
 */
public class Events {
	/**
	 * Event that is fired when Data is successfully updated from the server
	 */
	public static class OnDataUpdatedEvent {

	}

	/**
	 * Event that is fired when there is an error
	 */
	public static class OnErrorEvent {
		private final String mErrorMessage;

		/**
		 * Constructor
		 * @param errorMessage Error message
		 */
		public OnErrorEvent(final String errorMessage) {
			mErrorMessage = errorMessage;
		}

		/**
		 * Gets the error message
		 * @return Error message
		 */
		public String getErrorMessage() {
			return mErrorMessage;
		}
	}

	/**
	 * Event that is fired when there is a Network Communication Error
	 */
	public static class OnNetworkCommunicationErrorEvent extends OnErrorEvent {

		/**
		 * Constructor
		 * @param errorMessage Error Message
		 */
		public OnNetworkCommunicationErrorEvent(String errorMessage) {
			super(errorMessage);
		}
	}
}
