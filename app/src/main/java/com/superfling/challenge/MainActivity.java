package com.superfling.challenge;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.superfling.challenge.adapter.ImageAdapter;
import com.superfling.challenge.bus.Events;
import com.superfling.challenge.model.ImageData;
import com.superfling.challenge.tasks.ImageDataLoaderTask;
import com.superfling.challenge.tasks.ImageDataUpdateTask;

import java.util.List;

import de.greenrobot.event.EventBus;


/**
 * Main application Activity that displays the list of Images
 * @author Yanislav
 */
public class MainActivity extends BaseActivity {
	private static final String TAG = MainActivity.class.getSimpleName();
	/** Images Adapter */
	private ImageAdapter mAdapter;
	/** Images Loader async task */
	private ImageDataLoaderTask mImageDataLoaderTask;
	/** Image Data update async task */
	private ImageDataUpdateTask mImageDataUpdateTask;
	/** Action bar refresh item */
	private MenuItem mRefreshItem;

	/** The listener that is called when the data is loaded */
	private final ImageDataLoaderTask.IImageLoaderResultListener mDataLoadedResultListener = new ImageDataLoaderTask.IImageLoaderResultListener() {
		@Override
		public void onDataLoaded(List<ImageData> imageData) {
			Log.d(TAG, "IImageLoaderResultListener Loaded " + imageData.size() + " items");
			mImageDataLoaderTask = null;
			if (imageData != null) {
				mAdapter.setImageDatas(imageData);
			} else {
				Toast.makeText(MainActivity.this, R.string.main_activity_error_during_data_load, Toast.LENGTH_LONG).show();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		RecyclerView recyclerView = (RecyclerView) findViewById(R.id.images_recycler_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));

		mAdapter = new ImageAdapter();
		recyclerView.setAdapter(mAdapter);

		loadData();
		updateData();
	}

	/**
	 * Starts data update from the server
	 */
	private void updateData() {
		if (mImageDataUpdateTask == null) {
			mImageDataUpdateTask = new ImageDataUpdateTask(this);
			mImageDataUpdateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

			showProgressBar();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		mRefreshItem = menu.findItem(R.id.action_refresh);
		if (mImageDataUpdateTask != null) {
			showProgressBar();
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_refresh) {
			updateData();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		super.onPause();
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);
	}

	/**
	 * Event Handler for the event when 'Data is updated'
	 * @param event Event
	 */
	public void onEventMainThread(final Events.OnDataUpdatedEvent event) {
		Log.d(TAG, "Events.OnDataUpdatedEvent");
		updateFinished();
		loadData();
	}

	/**
	 * Event handler for the event {@link Events.OnNetworkCommunicationErrorEvent}
	 * @param event {@link Events.OnNetworkCommunicationErrorEvent}
	 */
	public void onEventMainThread(final Events.OnNetworkCommunicationErrorEvent event) {
		Log.d(TAG, "Events.OnNetworkCommunicationErrorEvent Error Message: " + event.getErrorMessage());
		updateFinished();
		Toast.makeText(this, event.getErrorMessage(), Toast.LENGTH_LONG).show();
	}

	/**
	 * Shows the loading progress bar in the Action bar
	 */
	private void showProgressBar() {
		if (mRefreshItem != null) {
			mRefreshItem.setActionView(new ProgressBar(this, null, android.R.attr.progressBarStyle));
			mRefreshItem.expandActionView();
		}
	}

	/**
	 * Method that has to be called once the Data Update is finished
	 */
	private void updateFinished() {
		mImageDataUpdateTask = null;
		if (mRefreshItem != null) {
			mRefreshItem.setActionView(null);
		}
	}

	/**
	 * Loads the data from the database and update the list
	 */
	private void loadData() {
		if (mImageDataLoaderTask == null) {
			mImageDataLoaderTask = new ImageDataLoaderTask(this, mDataLoadedResultListener);
			mImageDataLoaderTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
	}
}
