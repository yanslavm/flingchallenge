package com.superfling.challenge;

import android.app.Application;
import android.util.Log;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.NoSubscriberEvent;
import de.greenrobot.event.SubscriberExceptionEvent;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Challenge Application
 *
 * @author Yanislav Mihaylov
 */
public class ChallengeApplication extends Application {
	private static final String TAG = ChallengeApplication.class.getSimpleName();

	@Override
	public void onCreate() {
		super.onCreate();
		EventBus.getDefault().register(this);

		CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
				.setDefaultFontPath("fonts/Roboto-Regular.ttf")
				.setFontAttrId(R.attr.fontPath)
				.build());
	}

	/**
	 * Event handler for unhandled event bus events.
	 *
	 * @param event The unhandled event.
	 */
	public void onEvent(final NoSubscriberEvent event) {
		Log.w(TAG, "*** UNHANDLED EVENT *** : " + event);
	}

	/**
	 * Event handler for exceptions happened in event subscribers.
	 *
	 * @param event The exception event.
	 */
	public void onEvent(final SubscriberExceptionEvent event) {
		Log.e(TAG, "*** ERROR IN EVENT HANDLER *** : " + event.throwable);
	}
}
