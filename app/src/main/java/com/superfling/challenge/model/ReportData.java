package com.superfling.challenge.model;

/**
 * Report data container
 * @author Yanislav
 */
public class ReportData {
	/** User ID*/
	private int mUserId;
	/** User Name */
	private String mUserName;
	/** Number of posts */
	private int mPostsCount;
	/** Average image size in bytes */
	private int mAverageImageSize;
	/** Maximum image width */
	private int mMaxImageWidth;

	/**
	 * Gets User ID
	 * @return User ID
	 */
	public int getUserId() {
		return mUserId;
	}

	/**
	 * Sets User ID
	 * @param userId User Id
	 */
	public void setUserId(int userId) {
		mUserId = userId;
	}

	/**
	 * Gets User Name
	 * @return User Name
	 */
	public String getUserName() {
		return mUserName;
	}

	/**
	 * Sets User Name
	 * @param userName User Name
	 */
	public void setUserName(String userName) {
		mUserName = userName;
	}

	/**
	 * Gets the number of posts
	 * @return Number of posts
	 */
	public int getPostsCount() {
		return mPostsCount;
	}

	/**
	 * Sets the number of posts
	 * @param postsCount Number of posts
	 */
	public void setPostsCount(int postsCount) {
		mPostsCount = postsCount;
	}

	/**
	 * Gets the average image size in bytes
	 * @return Average image size in bytes
	 */
	public int getAverageImageSize() {
		return mAverageImageSize;
	}

	/**
	 * Sets the average image size in bytes
	 * @param averageImageSize Average image size in bytes
	 */
	public void setAverageImageSize(int averageImageSize) {
		mAverageImageSize = averageImageSize;
	}

	/**
	 * Gets the maximum image width
	 * @return Maximum image width
	 */
	public int getMaxImageWidth() {
		return mMaxImageWidth;
	}

	/**
	 * Sets the maximum image width
	 * @param maxImageWidth maximum image width
	 */
	public void setMaxImageWidth(int maxImageWidth) {
		mMaxImageWidth = maxImageWidth;
	}
}
