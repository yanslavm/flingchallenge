package com.superfling.challenge.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Yanislav
 */
public class ImageData {
	private enum JsonElement {
		ID,
		ImageID,
		Title,
		UserID,
		UserName
	}

	private int mId;
	private int mImageId;
	private String mTitle;
	private int mUserId;
	private String mUserName;
	private boolean mIsItemShown;
	private int mImageWidth;
	private int mImageHeight;
	private int mImageSize;

	/**
	 * Gets the Image data ID
	 * @return Image data ID
	 */
	public int getId() {
		return mId;
	}

	/**
	 * Sets the Image data ID
	 * @param id Image data ID
	 */
	public void setId(final int id) {
		mId = id;
	}

	/**
	 * Gets the Image ID via that the image could be retrieved from the server
	 * @return Image ID
	 */
	public int getImageId() {
		return mImageId;
	}


	/**
	 * Sets the Image ID via that the image could be retrieved from the server
	 * @param imageId Image ID
	 */
	public void setImageId(final int imageId) {
		mImageId = imageId;
	}

	/**
	 * Gets the image title
	 * @return Image title
	 */
	public String getTitle() {
		return mTitle;
	}

	/**
	 * Sets the image title
	 * @param title Image title
	 */
	public void setTitle(final String title) {
		mTitle = title;
	}

	/**
	 * Gets the User ID
	 * @return User ID
	 */
	public int getUserId() {
		return mUserId;
	}

	/**
	 * Sets the User ID
	 * @param userId User ID
	 */
	public void setUserId(final int userId) {
		mUserId = userId;
	}

	/**
	 * Gets the User Name
	 * @return User Name
	 */
	public String getUserName() {
		return mUserName;
	}

	/**
	 * Sets the User Name
	 * @param userName User Name
	 */
	public void setUserName(final String userName) {
		mUserName = userName;
	}

	/**
	 * Gets if the item is displayed already
	 * @return If the item is shown
	 */
	public boolean isItemShown() {
		return mIsItemShown;
	}

	/**
	 * Sets if the item is shown already
	 * @param isItemShown If the item is shown already
	 */
	public void setItemShown(final boolean isItemShown) {
		mIsItemShown = isItemShown;
	}

	/**
	 * Gets the original Image Width
	 * @return Image width in pixels
	 */
	public int getImageWidth() {
		return mImageWidth;
	}

	/**
	 * Sets the original Image Width in pixels
	 * @param imageWidth Original Image Width in pixels
	 */
	public void setImageWidth(int imageWidth) {
		mImageWidth = imageWidth;
	}

	/**
	 * Gets the original Image Height in pixels
	 * @return Original Image Height in pixels
	 */
	public int getImageHeight() {
		return mImageHeight;
	}

	/**
	 * Sets the original Image Height in pixels
	 * @param imageHeight Original Image Height in pixels
	 */
	public void setImageHeight(int imageHeight) {
		mImageHeight = imageHeight;
	}

	/**
	 * Gets the original image size in bytes
	 * @return Original image size in bytes
	 */
	public int getImageSize() {
		return mImageSize;
	}

	/** Sets the original image size in bytes
	 * @param imageSize Original image size in bytes
	 */
	public void setImageSize(int imageSize) {
		mImageSize = imageSize;
	}

	/**
	 * Parses the JSON Object and sets the object fields
	 * @param jsonObject {@link JSONObject} that contains the ImageData
	 * @throws JSONException Excption is thrown in case of JSON Parsing exception - for example if some of the fields
	 * are missing from the response
	 */
	public void parseJsonObject(final JSONObject jsonObject) throws JSONException {
		mId = jsonObject.getInt(JsonElement.ID.name());
		mImageId = jsonObject.getInt(JsonElement.ImageID.name());
		mTitle = jsonObject.optString(JsonElement.Title.name());
		mUserId = jsonObject.getInt(JsonElement.UserID.name());
		mUserName = jsonObject.optString(JsonElement.UserName.name());
	}
}
