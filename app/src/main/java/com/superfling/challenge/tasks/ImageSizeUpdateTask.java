package com.superfling.challenge.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.superfling.challenge.db.DatabaseAdapter;
import com.superfling.challenge.model.ImageData;

/**
 * Async task that updates the Image data in the database
 * @author Yanislav
 */
public class ImageSizeUpdateTask extends AsyncTask<Void, Void, Void> {
	/** Context */
	private final Context mContext;
	/** Image data that needs to be updated */
	private final ImageData mImageData;

	/**
	 * Constructor
	 * @param context Context
	 * @param imageData {@link ImageData} that needs to be updated
	 */
	public ImageSizeUpdateTask(final Context context, final ImageData imageData) {
		mContext = context;
		mImageData = imageData;
	}

	@Override
	protected Void doInBackground(Void... params) {
		DatabaseAdapter db = DatabaseAdapter.getInstance(mContext);
		db.updateImageData(mImageData);

		return null;
	}
}
