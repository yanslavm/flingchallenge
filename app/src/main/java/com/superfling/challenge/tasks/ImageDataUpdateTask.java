package com.superfling.challenge.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.superfling.challenge.Config;
import com.superfling.challenge.R;
import com.superfling.challenge.bus.Events;
import com.superfling.challenge.db.DatabaseAdapter;
import com.superfling.challenge.model.ImageData;
import com.superfling.challenge.utils.HttpCallHelper;
import com.superfling.challenge.utils.HttpResponse;
import com.superfling.challenge.utils.IHttpCallHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Async task that retrieves the data from the server and stores it in the local database
 * @author Yanislav
 */
public class ImageDataUpdateTask extends AsyncTask<Void, Void, Void> {
	private static final String TAG = "ImageDataUpdateTask";
	/** Context */
	private final Context mContext;
	/** Instance of {@link IHttpCallHelper} that makes the HTTP calls */
	private final IHttpCallHelper mHttpCallHelper;

	/**
	 * Constructor
	 * @param context Context
	 */
	public ImageDataUpdateTask(final Context context) {
		mContext = context;
		mHttpCallHelper = new HttpCallHelper(context);
	}

	/**
	 * Constructor that has to be used only from the tests
	 * @param context Context
	 * @param httpCallHelper Instance of {@link IHttpCallHelper}
	 */
	ImageDataUpdateTask(final Context context, IHttpCallHelper httpCallHelper) {
		mContext = context;
		mHttpCallHelper = httpCallHelper;
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			HttpResponse response = mHttpCallHelper.getData(Config.getImagesDataUrl());
			if (response.getStatusLine().getStatusCode() == 200) {
				List<ImageData> imageDatas = parseImageDatasFromJson(response.getData());
				DatabaseAdapter db = DatabaseAdapter.getInstance(mContext);
				db.saveImageDatas(imageDatas);

				EventBus.getDefault().post(new Events.OnDataUpdatedEvent());
			} else {
				EventBus.getDefault().post(new Events.OnNetworkCommunicationErrorEvent(mContext.getString(R.string.message_communication_error)));
			}
		} catch (IOException e) {
			Log.e(TAG, e.getMessage(), e);
			EventBus.getDefault().post(new Events.OnNetworkCommunicationErrorEvent(mContext.getString(R.string.message_network_error)));
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage(), e);
			EventBus.getDefault().post(new Events.OnNetworkCommunicationErrorEvent(mContext.getString(R.string.message_general_error)));
		} finally {
			mHttpCallHelper.close();
		}
		return null;
	}

	/**
	 * Parsing the json data received from the backend
	 * @param jsonData JSON representation of the data
	 * @return List of {@link ImageData}
	 * @throws JSONException When there is a problem with the data parsing
	 */
	private List<ImageData> parseImageDatasFromJson(String jsonData) throws JSONException {
		List<ImageData> result = new ArrayList<ImageData>();
		JSONArray array = new JSONArray(jsonData);
		for (int i = 0; i < array.length(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			try {
				ImageData imageData = new ImageData();
				imageData.parseJsonObject(jsonObject);
				result.add(imageData);
			} catch (JSONException e) {
				Log.e(TAG, "JSONException while parsing: " + jsonObject.toString(), e);
			}
		}

		return result;
	}
}
