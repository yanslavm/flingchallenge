package com.superfling.challenge.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.superfling.challenge.db.DatabaseAdapter;
import com.superfling.challenge.model.ReportData;

import java.util.List;

/**
 * Async task that generates the report
 * @author Yanislav
 */
public class ReportGeneratorTask extends AsyncTask<Void, Void, Void> {
	private static final String TAG = ReportGeneratorTask.class.getSimpleName();
	public static final String SEPARATOR_LINE = "********************************************************************************************************\n";
	public static final String HEADER_LINE    = "*                 User Name                * Posts Count *                      *                      *\n";

	/** Context */
	private Context mContext;

	/**
	 * Constructor
	 * @param context Application Context
	 */
	public ReportGeneratorTask(final Context context) {
		mContext = context;
	}

	@Override
	protected Void doInBackground(Void... params) {
		final DatabaseAdapter db = DatabaseAdapter.getInstance(mContext);
		final List<ReportData> reportDatas = db.generateReportData();
		final StringBuilder builder = new StringBuilder();
		builder.append("\n Report by users\n");
		builder.append(SEPARATOR_LINE);
		builder.append(HEADER_LINE);
		builder.append(SEPARATOR_LINE);

		for (final ReportData reportData: reportDatas) {
			builder.append(String.format("* %1$40s * %2$11s * %3$20s * %4$20s *\n", reportData.getUserName(),
					reportData.getPostsCount(), reportData.getAverageImageSize(), reportData.getMaxImageWidth()));

			builder.append(SEPARATOR_LINE);
		}

		Log.d(TAG, builder.toString());

		return null;
	}
}
