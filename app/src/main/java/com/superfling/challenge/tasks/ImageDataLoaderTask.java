package com.superfling.challenge.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.superfling.challenge.db.DatabaseAdapter;
import com.superfling.challenge.model.ImageData;

import java.util.List;

/**
 * Async task that loads the images from the Database
 * @author Yanislav
 */
public class ImageDataLoaderTask extends AsyncTask<Void, Void, Void> {
	/** Context */
	private final Context mContext;
	/** Result Listener that is called once the data is loaded */
	private final IImageLoaderResultListener mListener;
	/** Progress Bar that is shown while the data is loading*/
	private ProgressDialog mProgressDialog;
	/** List of {@link ImageData} that is loaded */
	private List<ImageData> mImageDatas;

	/**
	 * Result Listener Interface
	 */
	public interface IImageLoaderResultListener {
		/**
		 * Method that is called once the data is loaded
		 * @param imageData Loaded List of {@link ImageData}
		 */
		void onDataLoaded(List<ImageData> imageData);
	}

	/**
	 * Constructor
	 * @param context Context
	 * @param listener Result Listener that will be called once the data is loaded
	 */
	public ImageDataLoaderTask(final Context context, final IImageLoaderResultListener listener) {
		mContext = context;
		mListener = listener;
	}

	@Override
	protected void onPreExecute() {
		mProgressDialog = ProgressDialog.show(mContext, /* title */ null, /* text */ null);
	}

	@Override
	protected Void doInBackground(Void... params) {
		DatabaseAdapter db = DatabaseAdapter.getInstance(mContext);
		mImageDatas = db.getAllImageDatas();
		return null;
	}

	@Override
	protected void onPostExecute(Void aVoid) {
		if ((mProgressDialog != null) && (mProgressDialog.isShowing())) {
			mProgressDialog.dismiss();
		}

		mListener.onDataLoaded(mImageDatas);
	}
}
