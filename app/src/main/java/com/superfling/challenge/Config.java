package com.superfling.challenge;

/**
 * Application Configurations
 * @author Yanislav
 */
public class Config {
	private static final String BASE_URL = "http://challenge.superfling.com/";
	private static final String IMAGE_PATH = "photos/";

	/**
	 * Gets the Images Data URL that is used to retrieve images data
	 * @return Images Data URL
	 */
	public static String getImagesDataUrl() {
		return BASE_URL;
	}

	/**
	 * Gets the Image Download URL based on the Image ID
	 * @param imageId Image ID
	 * @return Image Download URL
	 */
	public static String getImageUrl(final int imageId) {
		return BASE_URL + IMAGE_PATH + String.valueOf(imageId);
	}
}
