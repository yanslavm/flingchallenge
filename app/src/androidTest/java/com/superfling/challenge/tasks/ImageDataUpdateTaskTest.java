package com.superfling.challenge.tasks;

import android.test.ActivityInstrumentationTestCase2;

import com.superfling.challenge.Config;
import com.superfling.challenge.R;
import com.superfling.challenge.TestActivity;
import com.superfling.challenge.bus.Events;
import com.superfling.challenge.utils.HttpResponse;
import com.superfling.challenge.utils.IHttpCallHelper;

import junit.framework.Assert;

import org.apache.http.Header;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.easymock.EasyMock;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import de.greenrobot.event.EventBus;

/**
 * Test for the {@link ImageDataUpdateTask} async task
 * @author Yanislav
 */
public class ImageDataUpdateTaskTest extends ActivityInstrumentationTestCase2<TestActivity> {
	private Object mLastEvent;
	private CountDownLatch mSignal;

	public ImageDataUpdateTaskTest() {
		super(TestActivity.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		mSignal = null;
		mLastEvent = null;
		EventBus.getDefault().register(this);

	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		EventBus.getDefault().unregister(this);
	}

	public void onEvent(Object event) {
		mLastEvent = event;
		if (mSignal != null) {
			mSignal.countDown();
		}
	}

	/**
	 * Tests that event will be fired in case of IOException
	 * @throws Exception
	 */
	public void testNetworkError() throws Exception {
		IHttpCallHelper callHelper = EasyMock.createMock(IHttpCallHelper.class);
		EasyMock.expect(callHelper.getData(Config.getImagesDataUrl())).andThrow(new IOException("TestIOException"));
		callHelper.close();
		EasyMock.expectLastCall();
		EasyMock.replay(callHelper);

		mSignal = new CountDownLatch(1);
		synchronized (mSignal) {
			ImageDataUpdateTask task = new ImageDataUpdateTask(getActivity(), callHelper);
			task.execute();
			mSignal.wait(500);
		}

		EasyMock.verify(callHelper);

		Assert.assertTrue("OnNetworkCommunicationErrorEvent expected", mLastEvent instanceof Events.OnNetworkCommunicationErrorEvent);
		Assert.assertEquals(getActivity().getString(R.string.message_communication_error), ((Events.OnNetworkCommunicationErrorEvent)mLastEvent).getErrorMessage());
	}

	/**
	 * Tests that event will be fired in case response code different than 200
	 * @throws Exception
	 */
	public void testWrongResponseCode() throws Exception {
		IHttpCallHelper callHelper = EasyMock.createMock(IHttpCallHelper.class);

		HttpResponse httpResponse = new HttpResponse(createStatuLine(301), new Header[0], "");
		makeCallAndVerifyMock(callHelper, httpResponse);

		Assert.assertTrue("OnNetworkCommunicationErrorEvent expected", mLastEvent instanceof Events.OnNetworkCommunicationErrorEvent);
		Assert.assertEquals(getActivity().getString(R.string.message_communication_error), ((Events.OnNetworkCommunicationErrorEvent)mLastEvent).getErrorMessage());
	}

	/**
	 * Tests that event will be fired in case a wrong data is received (JSON Parsing exception)
	 * @throws Exception
	 */
	public void testWrongData() throws Exception {
		IHttpCallHelper callHelper = EasyMock.createMock(IHttpCallHelper.class);

		HttpResponse httpResponse = new HttpResponse(createStatuLine(200), new Header[0], "{}");
		makeCallAndVerifyMock(callHelper, httpResponse);

		Assert.assertTrue("OnNetworkCommunicationErrorEvent expected", mLastEvent instanceof Events.OnNetworkCommunicationErrorEvent);
		Assert.assertEquals(getActivity().getString(R.string.message_communication_error), ((Events.OnNetworkCommunicationErrorEvent)mLastEvent).getErrorMessage());
	}


	/**
	 * Tests that successful data update event will be fired in case of success
	 * @throws Exception
	 */
	public void testSuccessfulCall() throws Exception {
		IHttpCallHelper callHelper = EasyMock.createMock(IHttpCallHelper.class);

		HttpResponse httpResponse = new HttpResponse(createStatuLine(200), new Header[0], "[{\"ID\":1, \"ImageID\":300,\n" +
				"    \"Title\":\"Acton Town, Ealing\",\n" +
				"    \"UserID\":10,\n" +
				"    \"UserName\":\"Benjamin\"}]");

		makeCallAndVerifyMock(callHelper, httpResponse);

		Assert.assertTrue("OnDataUpdatedEvent expected but it was " + mLastEvent.getClass().getSimpleName(), mLastEvent instanceof Events.OnDataUpdatedEvent);
	}

	/**
	 * Prepares the mocks triggers the Async Task and verifies the mock object
	 * @param callHelper Mock of the {IHttpCallHelper} interface
	 * @param httpResponse Expected response
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void makeCallAndVerifyMock(IHttpCallHelper callHelper, HttpResponse httpResponse) throws IOException, InterruptedException {
		EasyMock.expect(callHelper.getData(Config.getImagesDataUrl())).andReturn(httpResponse);
		callHelper.close();
		EasyMock.expectLastCall();
		EasyMock.replay(callHelper);

		mSignal = new CountDownLatch(1);
		synchronized (mSignal) {
			ImageDataUpdateTask task = new ImageDataUpdateTask(getActivity(), callHelper);
			task.execute();
			mSignal.wait(500);
		}

		EasyMock.verify(callHelper);
	}

	/**
	 * Creates a status line object that will return the status code that is passed as a parameter
	 * @param statusCode Status code of the status line
	 * @return
	 */
	private static StatusLine createStatuLine(final int statusCode) {
		return new StatusLine() {
			@Override
			public ProtocolVersion getProtocolVersion() {
				return null;
			}

			@Override
			public int getStatusCode() {
				return statusCode;
			}

			@Override
			public String getReasonPhrase() {
				return "";
			}
		};
	}
}
